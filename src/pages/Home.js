import React from 'react';
import Main from './Main';

const HomePage = () => (
  <Main>
    <h1>Customer Dashboard Section</h1>
  </Main>
)

export default HomePage;
