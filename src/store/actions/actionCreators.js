import { SET_CURRENT_USER, LOGOUT, GET_ERRORS } from "./types";

export function setCurrentUser(user) {
  return {
    type: SET_CURRENT_USER,
    payload: user
  }
}

export function logoutUser() {
  return {
    type: LOGOUT
  }
}

export function getErrors(errors) {
  return {
    type: GET_ERRORS,
    payload: errors
  }
}
